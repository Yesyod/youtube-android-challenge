package com.yesyod.youtubechallenge.adpters;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.yesyod.youtubechallenge.PlayVideoActivity;
import com.yesyod.youtubechallenge.R;
import com.yesyod.youtubechallenge.models.PlayListItem;


public class ChildViewHoilder extends ChildViewHolder {

    TextView title;
    ImageView thumb;

    public ChildViewHoilder(View itemView) {
        super(itemView);
        title = (TextView)itemView.findViewById(R.id.textView);
        thumb = (ImageView)itemView.findViewById(R.id.imageView);
    }
    public void bind(final Context context, final PlayListItem playlistItem){
           title.setText(playlistItem.getTitle());
            Glide.with(context).load(playlistItem.getThumb()).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    thumb.setImageDrawable(resource);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                }
            });
            Log.i("glide", "bind: "+playlistItem.getThumb());
            thumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(),PlayVideoActivity.class);
                    intent.putExtra(PlayVideoActivity.EXTRA_VIDEO_URL,playlistItem.getLink());
                    context.startActivity(intent);
                }
            });



    }

}
