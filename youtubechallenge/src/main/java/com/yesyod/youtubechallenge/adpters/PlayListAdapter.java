package com.yesyod.youtubechallenge.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.yesyod.youtubechallenge.R;
import com.yesyod.youtubechallenge.models.PlayList;
import com.yesyod.youtubechallenge.models.PlayListItem;

import java.util.ArrayList;


public class PlayListAdapter extends ExpandableRecyclerAdapter<PlayList,PlayListItem,
        PlaylistsViewHolder, ChildViewHoilder> {

    private Context mContext;
    private LayoutInflater mInflater;

    public PlayListAdapter(Context context, ArrayList<PlayList> playlistList) {
        super(playlistList);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    // onCreate ...
    @Override
    public PlaylistsViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup, int viewType) {
        View playlistView = mInflater.inflate(R.layout.playlist_view, parentViewGroup, false);
        return new PlaylistsViewHolder(playlistView);
    }

    @Override
    public ChildViewHoilder onCreateChildViewHolder( ViewGroup childViewGroup, int viewType) {
        View childView = mInflater.inflate(R.layout.child_view, childViewGroup, false);
        return new ChildViewHoilder(childView);
    }

    // onBind ...
    @Override
    public void onBindParentViewHolder(PlaylistsViewHolder playlistsViewHolder, int parentPosition, PlayList playlist) {
        playlistsViewHolder.bind(playlist);
    }

    @Override
    public void onBindChildViewHolder(ChildViewHoilder childViewHolder, int parentPosition, int childPosition,PlayListItem playlistItem) {
        childViewHolder.bind(mContext,playlistItem);
    }
}