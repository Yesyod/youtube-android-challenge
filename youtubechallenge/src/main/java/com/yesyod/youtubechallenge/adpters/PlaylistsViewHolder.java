package com.yesyod.youtubechallenge.adpters;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.yesyod.youtubechallenge.R;
import com.yesyod.youtubechallenge.models.PlayList;


public class PlaylistsViewHolder extends ParentViewHolder {

    private TextView sPlaylistText;
    public PlaylistsViewHolder( View itemView) {
        super(itemView);
        sPlaylistText = (TextView)itemView.findViewById(R.id.playlist_titel);
    }
    public void bind(PlayList playlist){
        sPlaylistText.setText(playlist.getListTitle());
    }
}
