package com.yesyod.youtubechallenge;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.yesyod.youtubechallenge.adpters.PlayListAdapter;
import com.yesyod.youtubechallenge.helpers.JSONParser;
import com.yesyod.youtubechallenge.models.PlayList;
import com.yesyod.youtubechallenge.models.PlayListItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {



    private String TAG;
    private JSONArray mPlaylists = null;
    private static String url = "http://www.razor-tech.co.il/hiring/youtube-api.json";
    private ProgressDialog pd;
    private RecyclerView mRecyclerView;
    private ArrayList<PlayList> playLists;
    private PlayListAdapter mPlayListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        playLists = new ArrayList<PlayList>();
        new AsyncData().execute();


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mPlayListAdapter = new PlayListAdapter(this, playLists);
        mRecyclerView.setAdapter(mPlayListAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    private class AsyncData extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try{
                // Creating new JSON Parser
                JSONParser jParser = new JSONParser();
                // Getting JSON from URL
                JSONObject json = jParser.getJSONFromUrl(url);
                mPlaylists = json.getJSONArray("Playlists");

                for(int i=0;i<mPlaylists.length();i++){
                    JSONObject playlistObject = (JSONObject)mPlaylists.get(i);
                    PlayList playlist = new PlayList(playlistObject.optString("ListTitle"));
                    Log.i(TAG, "doInBackground:   "+playlist.getListTitle());

                    JSONArray playlistItems = playlistObject.getJSONArray("ListItems");
                    Log.i(TAG, "doInBackground:playlistItems=  "+playlistItems);

                    for(int j=0;j<playlistItems.length();j++){
                        JSONObject playlistItemObject = (JSONObject)playlistItems.get(j);
                        String title = playlistItemObject.optString("Title");
                        String link = playlistItemObject.optString("link");
                        String thumb = playlistItemObject.optString("thumb");
                        PlayListItem playlistItem = new PlayListItem(title,link,thumb);
                        playlist.addItemArray(playlistItem);


                    }
                    playLists.add(playlist);

                }
            }catch (Exception e) {
                // e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            mPlayListAdapter.notifyParentDataSetChanged(true);
            Log.i(TAG, "onPostExecute: ");
            if (pd.isShowing()){
                pd.dismiss();
            }
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
