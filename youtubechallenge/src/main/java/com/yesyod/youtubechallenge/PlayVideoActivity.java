package com.yesyod.youtubechallenge;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by shayd on 07/05/2017.
 */

public class PlayVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

        private static final String API_KEY = "AIzaSyAPX4Eod6sCm8OnYGkqFuMsYJF60BZK7xc" ;
        public static final String EXTRA_VIDEO_URL = "video_id";
        private static final int LAST_PLAYED_POSITION = 1;
        private String videoId;
        private YouTubePlayer mYouTubePlayer;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_plays_video);

                init();

        }

        private void init() {
                videoId=getIntent().getStringExtra(EXTRA_VIDEO_URL);
                videoId = videoId.substring(32,videoId.length());
                YouTubePlayerView youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_player_view);
                        youTubePlayerView.initialize(API_KEY,this);
        }


        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if(!b){
                mYouTubePlayer = youTubePlayer;
                SharedPreferences VideoTimeMillis = getApplicationContext().getSharedPreferences("VIDEO_TIME",MODE_PRIVATE);
                int time=VideoTimeMillis.getInt(videoId,0);

                if(time == 0){
                youTubePlayer.loadVideo(videoId);
                }
                youTubePlayer.loadVideo(videoId,time);

                }
        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        }

        @Override
        public void onBackPressed() {
                SharedPreferences VideoTimeMillis = getApplicationContext().getSharedPreferences("VIDEO_TIME",MODE_PRIVATE);
                SharedPreferences.Editor editor = VideoTimeMillis.edit();
                try {
                editor.putInt(videoId, mYouTubePlayer.getCurrentTimeMillis());
                editor.commit();
                }catch (Exception e){
        }

        super.onBackPressed();
        }
}
