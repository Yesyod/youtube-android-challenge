package com.yesyod.youtubechallenge.models;

/**
 * Created by shayd on 07/05/2017.
 */

public class PlayListItem {
     private String title;
     private String link;
     private String thumb;

    public PlayListItem(String title, String link, String thumb) {
        this.title = title;
        this.link = link;
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
