package com.yesyod.youtubechallenge.models;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shayd on 07/05/2017.
 */

public class PlayList implements Parent<PlayListItem> {

    String ListTitle;
    ArrayList<PlayListItem> playListItems;

    public PlayList(String listTitle) {
        ListTitle = listTitle;
    }

    public String getListTitle() {
        return ListTitle;
    }

    public void setListTitle(String listTitle) {
        ListTitle = listTitle;
    }

    public void addItemArray(PlayListItem playlistItem){
        if (playListItems == null){
            playListItems = new ArrayList<>();
        }
        playListItems.add(playlistItem);
    }

    public ArrayList<PlayListItem> getPlayListItems() {
        return playListItems;
    }

    public void setPlayListItems(ArrayList<PlayListItem> playListItems) {
        this.playListItems = playListItems;
    }

    @Override
    public List<PlayListItem> getChildList() {
        return playListItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
