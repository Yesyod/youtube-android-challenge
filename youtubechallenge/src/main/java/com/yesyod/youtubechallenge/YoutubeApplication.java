package com.yesyod.youtubechallenge;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shayd on 07/05/2017.
 */

public class YoutubeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/cour.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
